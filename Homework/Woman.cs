﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Woman : Child
    {
        public string Profession { get; set; }
        public Woman(DateTime birthdate, string name, string prof) : base(birthdate, name)
        {
            this.Profession = prof;
        }
        public override object Clone()
        {
            Woman woman = new Woman(BirthDate, Name, Profession);
            woman.Parent = this.Parent;
            return woman;
        }
        public override void Description()
        {
            Console.WriteLine($"This is a {Profession}, the name is {Name}.");
        }
        public override HumanBeing MyClone()
        {
            Woman woman;
            string Json = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            woman = Newtonsoft.Json.JsonConvert.DeserializeObject<Woman>(Json);
            return woman;
        }
    }
}
