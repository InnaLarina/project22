﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    public class HumanBeing : ICloneable, IMyCloneable
    {
        public DateTime BirthDate { get; set; }
        public string Name { get; set; }
        public virtual object Clone()
        {
            return new HumanBeing(BirthDate, Name);
        }
        public HumanBeing(DateTime birthDate, string name)
        {
            BirthDate = birthDate;
            Name = name;
        }
        public int Age()
        {
            DateTime now = DateTime.Today;
            int age = now.Year - BirthDate.Year;
            if (BirthDate > now.AddYears(-age)) age--;
            return age;
        }
        public virtual void Description()
        {
            Console.WriteLine($"This is {Name}, he/she is {Age()}.");
        }
        public virtual HumanBeing MyClone()
        {
            HumanBeing humanBeing;
            string Json = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            humanBeing = Newtonsoft.Json.JsonConvert.DeserializeObject<HumanBeing>(Json);
            return humanBeing;
        }
    }
}
