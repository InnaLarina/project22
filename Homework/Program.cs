﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            /*Console.WriteLine("Created mother...");
            Woman mummy = new Woman(new DateTime(1973, 12, 12), "Inna", "their mother");
            mummy.Description();
            Console.WriteLine();

            Console.WriteLine("Created daugther...");
            Child babySophie = new Child(new DateTime(2007, 9, 8), "Sophie");
            babySophie.Parent = mummy;
            babySophie.Description();
            Console.WriteLine();

            Console.WriteLine("Created son as a copy of the daugther...");
            Child babyNick = (Child)babySophie.Clone();
            babyNick.Description();
            Console.WriteLine();

            Console.WriteLine("Inicialized the son...");
            babyNick.Name = "Nick";
            babyNick.BirthDate = new DateTime(2018, 5, 16);
            babyNick.Description();
            Console.WriteLine();

            Console.WriteLine("Created father as a copy of the mother...");
            Woman dad = (Woman)mummy.Clone();
            dad.Name = "Joe";
            dad.Profession = "the best dad";
            dad.Description();
            Console.WriteLine();

            Console.WriteLine("Changed Sophie's name parent and Nick's...");
            babySophie.Parent.Name = "Michele";
            babySophie.Description();
            babyNick.Description();*/

            Console.WriteLine("Created mother...");
            Woman mummy = new Woman(new DateTime(1973, 12, 12), "Inna", "their mother");
            mummy.Description();
            Console.WriteLine();

            Console.WriteLine("Created daugther...");
            Child babySophie = new Child(new DateTime(2007, 9, 8), "Sophie");
            babySophie.Parent = mummy;
            babySophie.Description();
            Console.WriteLine();

            Console.WriteLine("Created son as a copy of the daugther...");
            Child babyNick = (Child)babySophie.MyClone();
            babyNick.Description();
            Console.WriteLine();

            Console.WriteLine("Inicialized the son...");
            babyNick.Name = "Nick";
            babyNick.BirthDate = new DateTime(2018, 5, 16);
            babyNick.Description();
            Console.WriteLine();

            Console.WriteLine("Created father as a copy of the mother...");
            Woman dad = (Woman)mummy.MyClone();
            dad.Name = "Joe";
            dad.Profession = "the best dad";
            dad.Description();
            Console.WriteLine();

            Console.WriteLine("Changed Sophie's parent but not Nick's...");
            babySophie.Parent.Name = "Michele";
            babySophie.Description();
            babyNick.Description();

            watch.Stop();
            Console.WriteLine();
            Console.WriteLine($"Execution time in milliseconds: {watch.ElapsedMilliseconds}");


            Console.ReadKey();


        }
    }
}
