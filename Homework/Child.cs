﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Child : HumanBeing
    {

        public HumanBeing Parent { get; set; }

        public override void Description()
        {
            string parent = Parent == null ? "" : Parent.Name;
            Console.WriteLine($"This is a pretty baby {Name}, parent is {parent}, he/she is {Age()} yes old!");
        }

        public override HumanBeing MyClone()
        {
            Child child;
            string Json = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            child = Newtonsoft.Json.JsonConvert.DeserializeObject<Child>(Json);
            return child;
        }

        public override object Clone()
        {
            Child child = new Child(BirthDate, Name);
            child.Parent = this.Parent;
            return child;
        }
        public Child(DateTime birthDate, string name) : base(birthDate, name) { }


    }
}
